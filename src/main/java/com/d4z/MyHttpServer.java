package com.d4z;

import java.lang.reflect.Constructor;

import com.d4z.mapping.ActionHandlerMapping;
import com.d4z.request.builders.IActionHandlerBuilder;
import com.d4z.request.handlers.ApiAbstractHandler;
import com.d4z.request.interfaces.IAbstractGetHandler;
import com.d4z.request.interfaces.IAbstractPostHandler;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;


public class MyHttpServer extends AbstractVerticle {

	@Override
	public void start() throws Exception {
		super.start();
		HttpServerOptions httpOption = new HttpServerOptions();
		Router router = Router.router(this.vertx);

		router.route("/api/:action").handler(BodyHandler.create()).handler(rtx -> {
			final String action = rtx.pathParam("action");
			JsonObject response;
			int statusCode = 200;
			
			try {
				String lvClassBuilder = ActionHandlerMapping.INSTANCE.getBuilderMapping(rtx.request().method().name());
				String lvClassHandler = ActionHandlerMapping.INSTANCE.getHandlerMapping(action);
				
				Class<?> myClass = Class.forName(lvClassBuilder);
				Constructor<?> constructor = myClass.getConstructor(String.class);
				IActionHandlerBuilder lvBuilder = (IActionHandlerBuilder) constructor.newInstance(lvClassHandler);
				
				String lvContentType = rtx.request().headers().get("content-type");
				System.out.println(lvContentType);
				
				
				lvBuilder
						.setCookie(null)
						.setSession(null)
						.setContentType(lvContentType)
						.setMethod(rtx.request().method().name());
						
				if (rtx.request().method() == HttpMethod.GET) {
					String lvQueries = rtx.request().query();
					lvBuilder.setRequest(lvQueries);
				}
				else {
					lvBuilder.setRequest(rtx.getBody());
				}
				
				ApiAbstractHandler lvHandler = lvBuilder.build();
				
				switch (rtx.request().method().name()) {
					case "POST":
						if (lvHandler instanceof IAbstractPostHandler) {
							response = ((IAbstractPostHandler<JsonObject>) lvHandler).post();
							break;
						}
					case "GET":
						if (lvHandler instanceof IAbstractGetHandler) {
							response = ((IAbstractGetHandler<JsonObject>) lvHandler).get();
							break;			
						}
					default:
						throw new UnsupportedOperationException("Method not found!");
				}
			} catch (Exception e) {
				e.printStackTrace();
				response = new JsonObject().put("errors", e.getMessage());
				statusCode = 500;
			}
			
			rtx.response()
			 .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
			 .setStatusCode(statusCode)
			 .end(response.toString());
		});

		HttpServer server = vertx.createHttpServer(httpOption);
		server.requestHandler(router).listen(33000, res -> {
			if (res.succeeded()) {
				System.out.println("Started http://localhost:33000/api/");
			} else {
				res.cause().printStackTrace();
			}
		});
	}
}
