package com.d4z;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Context;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;

/**
 * @author MKS
 */
public class MainVerticle extends AbstractVerticle {
	private Logger LOGGER = LogManager.getLogger(MainVerticle.class);

	@Override
	public void init(Vertx vertx, Context context) {
		super.init(vertx, context);
	}

	@Override
	public void start() throws Exception {
		// Deploy the http server first.
		DeploymentOptions lvOpts = new DeploymentOptions().setConfig(config());
		vertx.deployVerticle(MyHttpServer.class.getName(),
			lvOpts, handler -> {
				LOGGER.info("Deloy service "
						+ MyHttpServer.class.getSimpleName() + " "
						+ (handler.succeeded() ? "successfully." : "fail."));

				if (handler.succeeded()) {} else {
					LOGGER.info("Caused by: " + handler.cause().getMessage());
					vertx.close();
				}
			}
		);

		LOGGER.info(this.getClass().getSimpleName() + " has started!");
	}

	@Override
	public void stop() throws Exception {
		System.out.println("Swagger Stopped");
	}
}
