package com.d4z.request;

import io.vertx.core.json.JsonObject;

public class ApiJsonBodyRequest {
	private JsonObject data;
	
	public ApiJsonBodyRequest(JsonObject pData) {
		this.data = pData;
	}

	public JsonObject getData() {
		return data;
	}

	public void setData(JsonObject data) {
		this.data = data;
	}
}
