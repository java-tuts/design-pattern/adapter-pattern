package com.d4z.request.adapters;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.d4z.helpers.XMLHelper;
import com.d4z.request.ApiJsonBodyRequest;
import com.d4z.request.ApiXmlBodyRequest;
import com.d4z.request.IApiJsonBodyRequest;

import io.vertx.core.json.JsonObject;

public class ApiXmlBodyRequestAdapter implements IApiJsonBodyRequest {
	private ApiXmlBodyRequest adaptee;

	public ApiXmlBodyRequestAdapter(ApiXmlBodyRequest adaptee) {
		this.adaptee = adaptee;
	}

	@Override
	public ApiJsonBodyRequest getData() {
		ApiJsonBodyRequest lvData = new ApiJsonBodyRequest(new JsonObject());
		XMLHelper xmlHelper = new XMLHelper();
		try {
			Document doc = xmlHelper.getDocumentBuilder().parse(xmlHelper.getInputStream(this.adaptee.getData()));
			doc.getDocumentElement().normalize();
			NodeList nodeLst = doc.getDocumentElement().getChildNodes();
			Map elemen = xmlHelper.getElementKeyValue(nodeLst);
			Iterator it = elemen.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pairs = (Map.Entry) it.next();
				lvData.getData().put(pairs.getKey().toString(), pairs.getValue());
			}
		} catch (SAXParseException e) {
			// log error
		} catch (SAXException e) {
			// log error
		} catch (IOException e) {
			// log error
		}
		
		return lvData;
	}
}
