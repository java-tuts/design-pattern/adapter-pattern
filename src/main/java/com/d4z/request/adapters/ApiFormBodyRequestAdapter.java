package com.d4z.request.adapters;

import java.util.Arrays;

import com.d4z.request.ApiFormBodyRequest;
import com.d4z.request.ApiJsonBodyRequest;
import com.d4z.request.IApiJsonBodyRequest;

import io.vertx.core.json.JsonObject;

public class ApiFormBodyRequestAdapter implements IApiJsonBodyRequest {
	private ApiFormBodyRequest adaptee;
	
	public ApiFormBodyRequestAdapter(ApiFormBodyRequest adaptee) {
		this.adaptee = adaptee;
	}
	
	@Override
	public ApiJsonBodyRequest getData() {
		ApiJsonBodyRequest lvData = new ApiJsonBodyRequest(new JsonObject());
		Arrays.asList(this.adaptee.getData().split("&")).forEach(query -> {
			lvData.getData().put(query.split("=")[0], query.split("=")[1]);
		});
		return lvData;
	}
}
