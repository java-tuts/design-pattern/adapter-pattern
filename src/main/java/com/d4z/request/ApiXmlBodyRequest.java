package com.d4z.request;

public class ApiXmlBodyRequest {
	private String data;
	
	public ApiXmlBodyRequest(String pData) {
		this.data = pData;
	}
	
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
}
