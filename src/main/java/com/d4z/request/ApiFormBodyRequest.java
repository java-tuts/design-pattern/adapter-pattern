package com.d4z.request;

public class ApiFormBodyRequest {
	private String data;
	
	public ApiFormBodyRequest(String pData) {
		this.data = pData;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
}
