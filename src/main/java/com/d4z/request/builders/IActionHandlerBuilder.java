package com.d4z.request.builders;

import com.d4z.request.handlers.ApiAbstractHandler;

import io.vertx.core.json.JsonObject;

public interface IActionHandlerBuilder {
	public <R> IActionHandlerBuilder setRequest(R pRequest);
	public IActionHandlerBuilder setMethod(String pMethod);
	public IActionHandlerBuilder setCookie(JsonObject pCookie);
	public IActionHandlerBuilder setSession(JsonObject pSession);
	public IActionHandlerBuilder setContentType(String pContentType);
	public ApiAbstractHandler build();
}
