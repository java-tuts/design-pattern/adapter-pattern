package com.d4z.request.builders;

import java.lang.reflect.Constructor;

import com.d4z.request.ApiFormBodyRequest;
import com.d4z.request.ApiJsonBodyRequest;
import com.d4z.request.ApiXmlBodyRequest;
import com.d4z.request.IApiJsonBodyRequest;
import com.d4z.request.adapters.ApiFormBodyRequestAdapter;
import com.d4z.request.adapters.ApiXmlBodyRequestAdapter;
import com.d4z.request.handlers.ApiAbstractHandler;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;

public class PostActionHandlerBuilder implements IActionHandlerBuilder {
	private ApiAbstractHandler handler;
	
	public PostActionHandlerBuilder(String pClassHandler) throws Exception {
		Class<?> myClass = Class.forName(pClassHandler);
		Constructor<?> constructor = myClass.getConstructor();
		handler = (ApiAbstractHandler) constructor.newInstance();
	}
	
	@Override
	public ApiAbstractHandler build() {
		return handler;
	}
	
	@Override
	public IActionHandlerBuilder setCookie(JsonObject pCookie) {
		handler.setCookie(pCookie);
		return this;
	}
	
	@Override
	public IActionHandlerBuilder setMethod(String pMethod) {
		handler.setMethod(pMethod);
		return this;
	}
	
	@Override
	public IActionHandlerBuilder setSession(JsonObject pSession) {
		handler.setSession(pSession);
		return this;
	}
	
	@Override
	public <R> IActionHandlerBuilder setRequest(R pRequest) {
		if (pRequest instanceof JsonObject) {
			handler.setRequest(new ApiJsonBodyRequest((JsonObject) pRequest));
		}
		else if (pRequest instanceof Buffer) {
			Buffer lvBodyBuffer = (Buffer) pRequest;
			
			if (handler.getContentType() != null) {
				if (handler.getContentType().equalsIgnoreCase("application/json")) {
					handler.setRequest(new ApiJsonBodyRequest(lvBodyBuffer.toJsonObject()));
				}
				else if (handler.getContentType().equalsIgnoreCase("application/xml")) {
					IApiJsonBodyRequest lvXmlBodyAdapter = new ApiXmlBodyRequestAdapter(new ApiXmlBodyRequest(lvBodyBuffer.toString()));
					handler.setRequest(lvXmlBodyAdapter.getData());
				}
				else if (handler.getContentType().equalsIgnoreCase("application/x-www-form-urlencoded")) {
					IApiJsonBodyRequest lvFormBodyAdapter = new ApiFormBodyRequestAdapter(new ApiFormBodyRequest(lvBodyBuffer.toString()));
					handler.setRequest(lvFormBodyAdapter.getData());
				}
			}
		}
		return this;
	}
	
	@Override
	public IActionHandlerBuilder setContentType(String pContentType) {
		handler.setContentType(pContentType);
		return this;
	}
}
