package com.d4z.request.builders;

import java.lang.reflect.Constructor;
import java.util.Arrays;

import com.d4z.request.ApiJsonBodyRequest;
import com.d4z.request.handlers.ApiAbstractHandler;

import io.vertx.core.json.JsonObject;

public class GetActionHandlerBuilder implements IActionHandlerBuilder {
	private ApiAbstractHandler handler;
	
	public GetActionHandlerBuilder(String pClassHandler) throws Exception {
		Class<?> myClass = Class.forName(pClassHandler);
		Constructor<?> constructor = myClass.getConstructor();
		handler = (ApiAbstractHandler) constructor.newInstance();
	}
	
	@Override
	public ApiAbstractHandler build() {
		return handler;
	}
	
	@Override
	public IActionHandlerBuilder setCookie(JsonObject pCookie) {
		handler.setCookie(pCookie);
		return this;
	}
	
	@Override
	public IActionHandlerBuilder setMethod(String pMethod) {
		handler.setMethod(pMethod);
		return this;
	}
	
	@Override
	public IActionHandlerBuilder setSession(JsonObject pSession) {
		handler.setSession(pSession);
		return this;
	}
	
	@Override
	public <R> IActionHandlerBuilder setRequest(R pRequest) {
		handler.setRequest(new ApiJsonBodyRequest(new JsonObject()));
		if (pRequest instanceof String) {
			Arrays.asList(((String) pRequest).split("&")).forEach(query -> {
				handler.getRequest().getData().put(query.split("=")[0], query.split("=")[1]);
			});
		}
		return this;
	}
	
	@Override
	public IActionHandlerBuilder setContentType(String pContentType) {
		handler.setContentType(pContentType);
		return this;
	}
}
