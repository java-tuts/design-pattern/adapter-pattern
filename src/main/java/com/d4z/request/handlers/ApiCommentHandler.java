package com.d4z.request.handlers;

import com.d4z.request.interfaces.IAbstractGetHandler;
import com.d4z.request.interfaces.IAbstractPostHandler;

import io.vertx.core.json.JsonObject;

public class ApiCommentHandler extends ApiAbstractHandler implements 
	IAbstractGetHandler<JsonObject>,
	IAbstractPostHandler<JsonObject>
{
	
	@Override
	public JsonObject get() {
		return new JsonObject().put("message", "/api/comment GET method").put("hello", getRequest().getData().getValue("name"));
	}
	
	@Override
	public JsonObject post() {
		return new JsonObject().put("message", "/api/user POST method").put("hello", getRequest().getData().getValue("name"));
	}
}
