package com.d4z.request.handlers;

import com.d4z.request.ApiJsonBodyRequest;

import io.vertx.core.json.JsonObject;

public abstract class ApiAbstractHandler {
	public JsonObject cookie;
	public JsonObject session;
	public String method;
	public String contentType;
	public ApiJsonBodyRequest request;
	public JsonObject response;

	public JsonObject getCookie() {
		return cookie;
	}

	public void setCookie(JsonObject cookie) {
		this.cookie = cookie;
	}

	public JsonObject getSession() {
		return session;
	}

	public void setSession(JsonObject session) {
		this.session = session;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public ApiJsonBodyRequest getRequest() {
		return request;
	}

	public void setRequest(ApiJsonBodyRequest request) {
		this.request = request;
	}

	public JsonObject getResponse() {
		return response;
	}

	public void setResponse(JsonObject response) {
		this.response = response;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
}
