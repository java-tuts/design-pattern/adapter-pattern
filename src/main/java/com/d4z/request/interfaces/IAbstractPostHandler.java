package com.d4z.request.interfaces;

public interface IAbstractPostHandler<T> {
	public T post();
}
