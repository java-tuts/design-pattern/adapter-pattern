package com.d4z.mapping;

import java.util.HashMap;
import java.util.Map;

public enum ActionHandlerMapping {
	INSTANCE;
	
	private Map<String, String> handlerMapping;
	private Map<String, String> builderMapping;
	
	ActionHandlerMapping() {
		handlerMapping = new HashMap<String, String>();
		handlerMapping.put("user", "com.d4z.request.handlers.ApiUserHandler");
		handlerMapping.put("comment", "com.d4z.request.handlers.ApiCommentHandler");
		

		builderMapping = new HashMap<String, String>();
		builderMapping.put("GET", "com.d4z.request.builders.GetActionHandlerBuilder");
		builderMapping.put("POST", "com.d4z.request.builders.PostActionHandlerBuilder");
	}
	
	public String getHandlerMapping(String pAction) {
		return handlerMapping.get(pAction);
	}
	
	public String getBuilderMapping(String pAction) {
		return builderMapping.get(pAction);
	}
}
